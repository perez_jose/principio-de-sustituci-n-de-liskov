/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principiodesustitucióndeliskov;

/**
 *
 * @author CompuStore
 */
public class Ropa {
    private String Tela ;
    private String Color;
    private String Marca;
    private String Talla;

    public String getTela() {
        return Tela;
    }

    public void setTela(String Tela) {
        this.Tela = Tela;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getTalla() {
        return Talla;
    }

    public void setTalla(String Talla) {
        this.Talla = Talla;
    }
    
    
}
